package filmoteca.eps.ua.es.filmoteca;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

public class FilmDataActivity extends AppCompatActivity {

    public static final String EXTRA_FILM_TITLE = "EXTRA_FILM_TITLE";
    public static final String EXTRA_FILM_INDEX = "EXTRA_FILM_INDEX";
    public static final int CODIGO_ACTIVIDAD_EDITAR = 1;
    int mIndicePeli;
    String URLpeli;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_film_data);

        Intent intent = getIntent();
        mIndicePeli = intent.getIntExtra(EXTRA_FILM_INDEX, -1);

        /*String nombrePeli = intent.getStringExtra("nombrePeli");
        TextView nombrePelicula = (TextView) findViewById(R.id.textView_nombrePeli);
        nombrePelicula.setText(nombrePeli);*/

        Button button_editarPelicula = (Button)findViewById(R.id.button_editar_pelicula);
        Button button_volverPrinc = (Button) findViewById(R.id.button_volver_principal);
        Button button_imdb = (Button)findViewById(R.id.button_imbd);
        Button button_location = (Button) findViewById(R.id.button_location);

        editarPelicula(button_editarPelicula);
        volverPrincipal(button_volverPrinc);
        IrSitioIMDB(button_imdb);
        irAMapa(button_location);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        pintarDatosPantalla();
    }

    private void irAMapa(Button button_location) {
        button_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(FilmDataActivity.this, LocationActivity.class);
                intent.putExtra(FilmDataActivity.EXTRA_FILM_INDEX, mIndicePeli);
                startActivity(intent);
            }
        });
    }

    void editarPelicula(Button button_editarPelicula) {
        button_editarPelicula.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(FilmDataActivity.this, FilmEditActivity.class);
                intent.putExtra(FilmDataActivity.EXTRA_FILM_INDEX, mIndicePeli);
                startActivityForResult(intent, CODIGO_ACTIVIDAD_EDITAR);
            }
        });
    }

    void volverPrincipal(Button button_volverPrinc) {
        button_volverPrinc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Intent intent = new Intent(FilmDataActivity.this, FilmListActivity.class);
                setResult(RESULT_OK);
                finish();
            }
        });
    }
    void IrSitioIMDB (Button boton) {
        boton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent_sitioIMDB = new Intent (Intent.ACTION_VIEW, Uri.parse(URLpeli));
                startActivity(intent_sitioIMDB);
            }
        });
    }
    public void onActivityResult(int resquestCode, int resultCode, Intent data)
    {
        switch (resultCode)
        {
            case RESULT_OK:
                if(resquestCode == CODIGO_ACTIVIDAD_EDITAR)
                {
                    pintarDatosPantalla();
                }
                Toast.makeText(FilmDataActivity.this, "Resultado OK", Toast.LENGTH_SHORT).show();
            break;
            case RESULT_CANCELED:
                Toast.makeText(FilmDataActivity.this, "Resultado cancelado", Toast.LENGTH_SHORT).show();
            break;

        }
    }

    private void pintarDatosPantalla(){

        List<Film> listaPeliculasAux = FilmDataSource.getFilms(/*getApplicationContext()*/);

        ImageView cartel = (ImageView) findViewById(R.id.imageView_cartel);
        TextView fecha =  (TextView) findViewById(R.id.textView_añoEstreno);
        TextView director =  (TextView) findViewById(R.id.textViewd_director);
        TextView genero =  (TextView) findViewById(R.id.textView_genero);
        TextView formato =  (TextView) findViewById(R.id.textView_formato);
        TextView titulo = (TextView) findViewById(R.id.textView_nombrePeli);

        Film film = listaPeliculasAux.get(mIndicePeli);

        cartel.setImageResource(film.imageResId);
        fecha.setText(Integer.toString(film.year));
        director.setText(film.director);
        titulo.setText(film.title);
        genero.setText(getResources().getStringArray(R.array.genero)[film.genre]);
        formato.setText(getResources().getStringArray(R.array.formato)[film.format]);
        URLpeli = film.imdbUrl;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpTo(this, new Intent(this, FilmListActivity.class));
                return true;
        }
        return false;
    }

}
