package filmoteca.eps.ua.es.filmoteca;

import android.content.Context;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

/**
 * Created by mastermoviles on 30/10/17.
 */

public class FilmsArrayAdapter extends ArrayAdapter<Film> {

    Context myContext;
    LayoutInflater inflater;
    List<Film> DataList;
    private  SparseBooleanArray mSelectedItemsIds;

    public FilmsArrayAdapter(Context context, int resource, List<Film> objects){
        super(context, resource, objects);
        mSelectedItemsIds = new SparseBooleanArray();
        myContext = context;
        DataList = objects;
        inflater =  LayoutInflater.from(context);
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        if(convertView == null)
        {
            convertView = LayoutInflater.from(this.getContext()).inflate(R.layout.item_film, parent, false);
        }
        TextView FLnombre = (TextView) convertView.findViewById(R.id.nombrePelicula);
        TextView FLdirector = (TextView) convertView.findViewById(R.id.nombreDirector);
        ImageView cartel = (ImageView) convertView.findViewById(R.id.portadaPelicula);

        Film fl = getItem(position);
        FLnombre.setText(fl.title);
        FLdirector.setText(fl.director);
        cartel.setImageResource(fl.imageResId);

        return convertView;
    }
}
