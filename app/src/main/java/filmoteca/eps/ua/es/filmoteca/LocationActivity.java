package filmoteca.eps.ua.es.filmoteca;

import android.app.FragmentTransaction;
import android.content.Intent;
import android.graphics.Camera;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.List;

public class LocationActivity extends FragmentActivity implements OnMapReadyCallback {

    public static final String EXTRA_FILM_INDEX = "EXTRA_FILM_INDEX";
    int mIndicePeli;
    private MapFragment mMapFragment;
    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location);

        ///////// ------ No se si este trozo va aqui

        /*mMapFragment = MapFragment.newInstance(opcionesDeMapa);
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.miMapaContainer, mMapFragment);
        fragmentTransaction.commit();
        mMapFragment.getMapAsync(this);*/



        ///// ------- fin del trozo que no se si va aqui
        MapFragment mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.miMapaContainer);
        mapFragment.getMapAsync(this);

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        Intent intent = getIntent();
        mIndicePeli = intent.getIntExtra(EXTRA_FILM_INDEX, -1);

        List<Film> listaPeliculasAux = FilmDataSource.getFilms(/*getApplicationContext()*/);
        Film film = listaPeliculasAux.get(mIndicePeli);
        double latitudPeli =  film.getLatitud();
        double longitudPeli =  film.getLongitud();
        String titulo = film.title;
        String director = film.director;
        String fecha = film.year+"";


        mMap = googleMap;
        GoogleMapOptions opcionesDeMapa = new GoogleMapOptions();
        opcionesDeMapa.mapType(GoogleMap.MAP_TYPE_NORMAL).compassEnabled(false).rotateGesturesEnabled(true).tiltGesturesEnabled(false);
        //mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);


        //Add a marker in anywhere you want
        LatLng coordenada = new LatLng(latitudPeli, longitudPeli);
        mMap.addMarker(new MarkerOptions().position(coordenada).title(titulo).snippet(director + ", "+fecha));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(coordenada));
    }
}
