package filmoteca.eps.ua.es.filmoteca;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.github.scribejava.apis.TwitterApi;
import com.github.scribejava.core.builder.ServiceBuilder;
import com.github.scribejava.core.model.OAuth1AccessToken;
import com.github.scribejava.core.model.OAuth1RequestToken;
import com.github.scribejava.core.model.OAuthRequest;
import com.github.scribejava.core.model.Response;
import com.github.scribejava.core.model.Verb;
import com.github.scribejava.core.oauth.OAuth10aService;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.IOException;
import java.util.concurrent.ExecutionException;

import static android.R.attr.button;

public class AboutActivity extends AppCompatActivity {

    private final String TAG = "TwitterDebug";
    private final String nombreSharedPref = "PreferenciasTwitterJose";

    private final String APIkey ="cfBOYJDeo6200Q1GkxELMkoy1"; //api key
    private final String APIsecret ="jidZKTLNTQpuXpAupcoG0tCiGwtH7fgvgLN6PAFfhpjI9aOdU7"; //api secret
    public static final String PROTECTED_RESOURCE_URL = "https://api.twitter.com/1.1/statuses/update.json";

    private  String i_PREFERENCIA_ACCESS_SECRET = null;
    private  String i_PREFERENCIA_ACCESS_TOKEN = null;


    private final OAuth10aService service = new ServiceBuilder(APIkey)
            .apiSecret(APIsecret)
            .callback("app://twitter")
            .build(TwitterApi.instance());

    private static OAuth1RequestToken requestToken;
    Boolean conectado = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        Button button_irAlSitioWeb = (Button)findViewById(R.id.button_ir_sitio_web);
        Button button_obtenerSoporte = (Button)findViewById(R.id.button_obtener_soporte);
        Button button_volver = (Button)findViewById(R.id.button_volver);

        ObtenerSoporte(button_obtenerSoporte);
        IrAlSitioWeb(button_irAlSitioWeb);//click en botón "ir al sitio web"
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Volver(button_volver);

    }

    @Override
    protected void onResume() {
        super.onResume();

        GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(this);
        updateUI(account);
    }

    private void updateUI(GoogleSignInAccount account) {
        if(account != null){
            TextView nombreUsus = (TextView) findViewById(R.id.textView_nombreUsu);
            nombreUsus.setText(account.getDisplayName().toString());
        }
    }

    void IrAlSitioWeb (Button boton) {
        boton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent_sitioWeb = new Intent (Intent.ACTION_VIEW, Uri.parse("http://3.bp.blogspot.com/-lO2lI_r4BbA/UWS7Q9R_i-I/AAAAAAAAAFw/2P_7qix_1UM/s1600/bg_hola.png"));
                startActivity(intent_sitioWeb);
            }
        });
    }
    void ObtenerSoporte (Button boton) {
        boton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent_soporte = new Intent (Intent.ACTION_SENDTO, Uri.parse("mailto:@brujosemanuel@gmail.com"));
                startActivity(intent_soporte);
            }
        });
    }

    void Volver (Button boton) {
        boton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpTo(this, new Intent(this, FilmListActivity.class));
                return true;
        }
        return false;
    }

}
