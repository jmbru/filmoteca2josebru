package filmoteca.eps.ua.es.filmoteca;

import java.io.Serializable;

/**
 * Created by mastermoviles on 16/10/17.
 */

public class Film implements Serializable{

    //serializamos la clase añadiendo los getter y setter

    public final static int FORMAT_DVD = 0; //formatos
    public final static int FORMAT_BLURAY = 1;
    public final static int FORMAT_DIGITAL = 2;

    public final static int GENRE_ACTION = 0; //generos
    public final static int GENRE_COMEDY = 1;
    public final static int GENRE_DRAMA = 2;
    public final static int GENRE_SCIFI = 3;
    public final static int GENRE_HORROR = 4;

    public int imageResId;      //Propiedades de la clase

    public String title;

    public String director;

    public int year;

    public int genre;

    public int format;

    public String imdbUrl;

    public String comments;

    public double latitud;

    public double longitud;


    // ------ GETTER Y SETTER

    public double getLatitud() {
        return latitud;
    }

    public double getLongitud() {
        return longitud;
    }

    public void setLatitud(double latitud) {
        this.latitud = latitud;
    }

    public void setLongitud(double longitud) {
        this.longitud = longitud;
    }

    public int getImageResId() {
        return imageResId;
    }

    public String getTitle() {
        return title;
    }

    public String getDirector() {
        return director;
    }

    public int getYear() {
        return year;
    }

    public int getGenre() {
        return genre;
    }

    public int getFormat() {
        return format;
    }

    public String getImdbUrl() {
        return imdbUrl;
    }

    public String getComments() {
        return comments;
    }

    public void setImageResId(int imageResId) {
        this.imageResId = imageResId;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public void setGenre(int genre) {
        this.genre = genre;
    }

    public void setFormat(int format) {
        this.format = format;
    }

    public void setImdbUrl(String imdbUrl) {
        this.imdbUrl = imdbUrl;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String toString(){
        return title;       //Al convertir a cadena mostramos su título
    }



}
