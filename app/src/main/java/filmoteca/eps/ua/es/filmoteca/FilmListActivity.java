package filmoteca.eps.ua.es.filmoteca;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.github.scribejava.apis.TwitterApi;
import com.github.scribejava.core.builder.ServiceBuilder;
import com.github.scribejava.core.model.OAuth1AccessToken;
import com.github.scribejava.core.model.OAuth1RequestToken;
import com.github.scribejava.core.model.OAuthRequest;
import com.github.scribejava.core.model.Response;
import com.github.scribejava.core.model.Verb;
import com.github.scribejava.core.oauth.OAuth10aService;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.drive.Drive;
import com.google.android.gms.drive.DriveClient;
import com.google.android.gms.drive.DriveContents;
import com.google.android.gms.drive.DriveFile;
import com.google.android.gms.drive.DriveFolder;
import com.google.android.gms.drive.DriveResourceClient;
import com.google.android.gms.drive.Metadata;
import com.google.android.gms.drive.MetadataBuffer;
import com.google.android.gms.drive.MetadataChangeSet;
import com.google.android.gms.drive.query.Filters;
import com.google.android.gms.drive.query.Query;
import com.google.android.gms.drive.query.SearchableField;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.Tasks;

import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static filmoteca.eps.ua.es.filmoteca.FilmDataActivity.EXTRA_FILM_INDEX;

public class FilmListActivity extends AppCompatActivity {

    private final String TAG = "TwitterDebug";
    private final String nombreSharedPref = "PreferenciasTwitterJose";
    Boolean conectado = false;
    String textoTweet ="Mis peliculas favoritas son: ";

    private final String APIkey ="cfBOYJDeo6200Q1GkxELMkoy1"; //api key
    private final String APIsecret ="jidZKTLNTQpuXpAupcoG0tCiGwtH7fgvgLN6PAFfhpjI9aOdU7"; //api secret
    public static final String PROTECTED_RESOURCE_URL = "https://api.twitter.com/1.1/statuses/update.json";

    private final OAuth10aService service = new ServiceBuilder(APIkey)
            .apiSecret(APIsecret)
            .callback("app://twitter")
            .build(TwitterApi.instance());

    private static OAuth1RequestToken requestToken;


/// DRIVE -------------------------------
    private static final int REQUEST_CODE_SIGN_IN_DRIVE = 1;
    private static final String TAG1 = "Message:" ;
    //RecyclerView mRecyclerView;
    //RecyclerView.LayoutManager mLayoutManager;
    FilmsArrayAdapter adaptador;
    ListView listViewExterna;
    private GoogleSignInClient mGoogleSignInClient;
    private DriveClient mDriveClient;
    private DriveResourceClient mDriveResourceClient;
    private DriveFile mDriveFile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_film_array);

        //google account----
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestEmail().build();
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

        //drive
        Set<Scope> requiredScopes = new HashSet<>(2);
        requiredScopes.add(Drive.SCOPE_FILE);
        requiredScopes.add(Drive.SCOPE_APPFOLDER);
        GoogleSignInAccount signInAccount = GoogleSignIn.getLastSignedInAccount(this);

        if(signInAccount != null && signInAccount.getGrantedScopes().containsAll(requiredScopes)){
            initializeDriveClient(signInAccount);
            //createFileInAppFolder();
            findDriveFile();

        }else{
            GoogleSignInOptions signInOptions = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                    .requestScopes(Drive.SCOPE_FILE)
                    .requestScopes(Drive.SCOPE_APPFOLDER)
                    .build();
            //GoogleSignInClient googleSignInClient  = GoogleSignIn.getClient(this, signInOptions);
            GoogleSignInClient googleSignInClient  = GoogleSignIn.getClient(this, signInOptions);
            startActivityForResult(googleSignInClient.getSignInIntent(), REQUEST_CODE_SIGN_IN_DRIVE);
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode){
            case REQUEST_CODE_SIGN_IN_DRIVE:
                if(resultCode != RESULT_OK){
                    finish();
                    return;
                }else{
                    Task<GoogleSignInAccount> getAccountTask = GoogleSignIn.getSignedInAccountFromIntent(data);
                    if(getAccountTask.isSuccessful()){
                        initializeDriveClient(getAccountTask.getResult());
                        findDriveFile();
                    }
                    else{
                        finish();
                    }
                }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        final Uri uri = this.getIntent().getData();
        SharedPreferences settings = getSharedPreferences(nombreSharedPref, 0);


        if(settings.getString("accessToken", null) != null && settings.getString("accessSecret", null)!=null){
            Log.d(TAG, "onResume: accessToken y accessSecret en SharedPreferences");
            conectado=true;
        }
        else{
            // if shared settings are not set / check whether the uri is valid to do an OAuth Dance
            if (uri != null && uri.toString().startsWith("app://twitter")) {
                Log.d(TAG, "onResume: El usuario ha respondido y vuelve a invocar a la aplicación");
                String verifier = uri.getQueryParameter("oauth_verifier");
                if (verifier != null) {
                    Log.d(TAG, "onResume: El usuario ha autorizado el acceso en su nombre y se nos devuelve el verificador");
                    new OauthEnd().execute(verifier);
                    new enviarTweet().execute();
                } else
                    Log.d(TAG, "onResume: El usuario NO ha autorizado el acceso en su nombre");
            }
        }

    }

    public void findDriveFile(){
       final Task<DriveFolder> appFolderTask = mDriveResourceClient.getAppFolder();

       appFolderTask.continueWithTask(new Continuation<DriveFolder, Task<Void>>() {
           @Override
           public Task<Void> then(@NonNull Task<DriveFolder> task) throws Exception {

           final DriveFolder appFolder =  task.getResult();
           final Task<MetadataBuffer> metadaDataTask = mDriveResourceClient.listChildren(appFolder);

           metadaDataTask.continueWithTask(new Continuation<MetadataBuffer, Task<Void>>() {
               @Override
               public Task<Void> then(@NonNull Task<MetadataBuffer> task) throws Exception {
                   boolean findedFile =  false;
                   if(task.getResult().getCount()>0)
                   {
                       for (Metadata oneMetadata: task.getResult()){
                           if (findedFile==false){
                               //String prueba = oneMetadata.getTitle();
                               if(oneMetadata.getTitle().equals("films")){

                                   //asignamos el fichero de drive encontrado
                                   mDriveFile =  oneMetadata.getDriveId().asDriveFile();

                                   findedFile =  true;
                                   break;
                               }
                           }
                       }

                       if(findedFile){
                           //si encontramos el fichero lo leeremos
                           readDriveFileFilms();
                       }
                       else{
                           createFileInAppFolder();
                       }
               }else{
                       Log.e(TAG1, "creando el fichero por falta de uno");
                       createFileInAppFolder();
                       readDriveFileFilms();
                   }
                   return null;
               }
           });
           return null;
           }
       });
    }

    public void createFileInAppFolder(/*GoogleSignInAccount signInAccount*/) {

        final Task<DriveFolder> appFolderTask = mDriveResourceClient.getAppFolder();
        final Task<DriveContents> createContentsTask = mDriveResourceClient.createContents();
        Tasks.whenAll(appFolderTask, createContentsTask)
                .continueWithTask(new Continuation<Void, Task<DriveFile>>() {
                    @Override
                    public Task<DriveFile> then(@NonNull Task<Void> task) throws Exception {
                        DriveFolder parent = appFolderTask.getResult();
                        DriveContents contents = createContentsTask.getResult();
                        //OutputStream outputStream = contents.getOutputStream();
                        ObjectOutputStream outputStream1 = new ObjectOutputStream(contents.getOutputStream());

                        try{
                            outputStream1.writeObject(FilmDataSource.pelisDefault);
                        }finally {
                            outputStream1.close();
                        }

                        MetadataChangeSet changeSet = new MetadataChangeSet.Builder()
                                .setTitle("films")
                                .setMimeType("text/plain")
                                .setStarred(true)
                                .build();

                        return mDriveResourceClient.createFile(parent, changeSet, contents);
                    }
                }).addOnSuccessListener(this, new OnSuccessListener<DriveFile>() {
            @Override
            public void onSuccess(DriveFile driveFile) {
               // finish();
                findDriveFile();

                Log.i(TAG1, "file created: " + driveFile.getDriveId().encodeToString());
            }
        }).addOnFailureListener(this, new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.e(TAG1, "unable to create file", e);
                //finish();
            }
        });

    }

    private void readDriveFileFilms(/*Task<MetadataBuffer> task1, int i*/) {
        Task<DriveContents> openFileTask = mDriveResourceClient.openFile(mDriveFile, DriveFile.MODE_READ_ONLY);
        //String titulo =  task1.getResult().get(i).getTitle().toString();
        openFileTask.continueWithTask(new Continuation<DriveContents, Task<Void>>() {
            @Override
            public Task<Void> then(@NonNull Task<DriveContents> task) throws Exception {
                DriveContents contents = task.getResult();
                ObjectInput ois = new ObjectInputStream(contents.getInputStream());
                List<Film> films = new ArrayList<Film>();
                try{
                    films = (List<Film>) ois.readObject();
                }finally {
                    ois.close();
                }
                FilmDataSource.setFilms(films);
                crearAdapter(films);
                Task<Void> discardTask = mDriveResourceClient.discardContents(contents);
                return discardTask;
            }
        });
    }

    private void writeDriveFileFilms(/*Task<MetadataBuffer> task1, int i*/) {
        Task<DriveContents> openFileTask = mDriveResourceClient.openFile(mDriveFile, DriveFile.MODE_WRITE_ONLY);

        openFileTask.continueWithTask(new Continuation<DriveContents, Task<Void>>() {
            @Override
            public Task<Void> then(@NonNull Task<DriveContents> task) throws Exception {
                DriveContents contents = task.getResult();
                ObjectOutputStream oos = new ObjectOutputStream(contents.getOutputStream());
                List<Film> films = FilmDataSource.getFilms();
                try{
                    oos.writeObject(films);
                }finally {
                    oos.close();
                }

                MetadataChangeSet changeSet = new MetadataChangeSet.Builder()
                        .setStarred(true)
                        .setLastViewedByMeDate(new Date())
                        .build();

                Task<Void> commitTask = mDriveResourceClient.commitContents(contents, changeSet);
                return commitTask;
            }
        });
    }

    private void crearAdapter(List<Film> films) {
        adaptador = new FilmsArrayAdapter(this, R.layout.item_film, films);
        listViewExterna =(ListView) findViewById(R.id.listViewArrayFilm);
        //setListAdapter(adaptador);
        listViewExterna.setAdapter(adaptador);

        listViewExterna.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                Intent intent = new Intent(FilmListActivity.this, FilmDataActivity.class);
                intent.putExtra(EXTRA_FILM_INDEX, position);
                startActivity(intent);
            }
        });

        listViewExterna.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);

        listViewExterna.setMultiChoiceModeListener(
            new AbsListView.MultiChoiceModeListener() {

                @Override
                public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                    MenuInflater inflater = mode.getMenuInflater();
                    inflater.inflate(R.menu.menu_borrado, menu);
                    return true;
                }

                @Override
                public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                    return false;
                }

                @Override
                public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                    switch (item.getItemId()) {
                        case R.id.borrado:
                            borrarPeliculasSeleccionadas();
                            mode.finish();
                            return true;
                        case R.id.publicar:
                            publicarPeliculasFavoritas();
                            mode.finish();
                            return true;
                        default:
                            return false;
                    }
                }
                public void publicarPeliculasFavoritas(){
                    SparseBooleanArray indices = listViewExterna.getCheckedItemPositions();
                    List<Film> films = FilmDataSource.getFilms();

                    SharedPreferences settings = getSharedPreferences(nombreSharedPref, 0);
                    final SharedPreferences.Editor editor = settings.edit();

                    String completador = "[...]";
                    int tamMax = 240;
                    if (indices != null)
                    {
                        for(int i=0; i<indices.size(); i++){

                            textoTweet = textoTweet + films.get(indices.keyAt(i)).title;
                            if(i!= indices.size()-1){
                                textoTweet = textoTweet + ", ";
                            }

                            Log.e("INDICE", "Indice de la lista: " + films.get(indices.keyAt(i)).title);
                        }
                    }
                    if(textoTweet.length()>tamMax){
                        textoTweet = textoTweet.substring(0, 235) + completador;
                    }

                    //guardamos el tweet en una shared para no perderlo
                    editor.putString("textoTweet", textoTweet);
                    editor.apply();

                    if (conectado) {
                        new enviarTweet().execute();
                    }
                    else{
                        String authUrl = null;
                        try {
                            authUrl = new authUrl().execute().get();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        Log.d(TAG, "Solcitud de autorización: Una vez obtenidas las claves temporales, manda a que el usuario autorize el acceso");
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(authUrl)));
                    }
                }
                @Override
                public void onDestroyActionMode(ActionMode mode) {
                }
                @Override
                public void onItemCheckedStateChanged(ActionMode mode, int position, long id, boolean checked) {
                    int num_items_seleccionados;
                    String texto;

                    num_items_seleccionados = listViewExterna.getCheckedItemCount();
                    texto = "seleccionados";
                    mode.setTitle(num_items_seleccionados + " " + texto);
                }
            });
    }

    //Si llegamos aqui es que estamos conectados a gmail
    private void initializeDriveClient(GoogleSignInAccount signInAccount) {
        mDriveClient = Drive.getDriveClient(getApplicationContext(), signInAccount);
        mDriveResourceClient = Drive.getDriveResourceClient(getApplicationContext(), signInAccount);
    }



    private void borrarPeliculasSeleccionadas() {

        SparseBooleanArray indices = listViewExterna.getCheckedItemPositions();
        List<Film> pelisParaBorrar = new ArrayList<Film>();
        for(int i=0; i<indices.size(); i++) {
            if(indices.valueAt(i)) {
                pelisParaBorrar.add(FilmDataSource.films.get(indices.keyAt(i)));
            }
        }
        FilmDataSource.guardarPeliculasFichero(getApplicationContext());
    }




    //----------------------------------TWITTER---------------------------------

    //------------------------------------------------- --------------------------- ---------------------

    private class OauthEnd extends AsyncTask<String, Void, Void> {
        @Override
        protected Void doInBackground(String... params) {
            SharedPreferences settings = getSharedPreferences(nombreSharedPref, 0);
            final SharedPreferences.Editor editor = settings.edit();

            final String verifier = params[0];
            Log.d(TAG, "OauthEnd: Una vez con el verificador, solicitamos el Token de acceso");
            try {
                // Setup storage for access token
                final OAuth1AccessToken accessToken = service.getAccessToken(requestToken, verifier);

                Log.d(TAG, "OauthEnd: Deveulto el Token de acceso, lo guardamos en las SharedPreferences para tenerlo disponible para hacer peticiones");
                editor.putString("accessToken", accessToken.getToken());
                editor.putString("accessSecret", accessToken.getTokenSecret());

                editor.apply();
            } catch (Exception e) {
                Log.d(TAG, "Excepción producidad al intentar obtener el accessToken");
            }
            return null;
        }
    }

//--------
    private class enviarTweet extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            SharedPreferences settings = getSharedPreferences(nombreSharedPref, 0);
            String respuesta = null;

            Log.d(TAG, "getUser: Hacemos una petición de ejemplo con el Token de acceso que tenemos");
            OAuth1AccessToken newAccessToken = new OAuth1AccessToken(settings.getString("accessToken", null), settings.getString("accessSecret", null));
            final OAuthRequest request = new OAuthRequest(Verb.POST, PROTECTED_RESOURCE_URL);

            request.addQuerystringParameter("status", settings.getString("textoTweet", "tweet generico"));

            service.signRequest(newAccessToken, request);

            try {
                final Response response = service.execute(request);
                if (response.isSuccessful()) {
                    Log.d(TAG, "La respuesta a la solicitud es satisfactoria");
                    //respuesta = response.getBody();

                } else {
                    Log.d(TAG, "La respuesta a la solicitud NO es satisfactoria");
                    respuesta = "-1";
                }
            } catch (Exception e) {
                Log.d(TAG, "Excepción producida al ejecutar la solicitud de acceso al recurso protegido");
            }
            return respuesta;
        }

        @Override
        protected void onPostExecute(String result) {
            if(result != null && result.equals("-1")){
                Log.d(TAG, "Problemas al enviar el tweet");
                Toast toast1 = Toast.makeText(getApplicationContext(), "Problemas al enviar el tweet", Toast.LENGTH_SHORT);
                toast1.show();
            }else{
                Log.d(TAG, "Tweet enviado");
                Toast toast2 = Toast.makeText(getApplicationContext(), "Tweet enviado correctamente", Toast.LENGTH_SHORT);
                toast2.show();
                conectado=true;
            }
        }
    }


    private class authUrl extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            try {
                Log.d(TAG, "authUrl: Solicitamos las credenciales temporales");
                requestToken = service.getRequestToken();
            } catch (Exception e) {
                e.printStackTrace();
            }

            Log.d(TAG, "authUrl: Componemos la URL a la que mandar al usuario para que autorize el acceso");
            return service.getAuthorizationUrl(requestToken);
        }

    }

    //------------------------------------------------- --------------------------- ---------------------


    //-------------------------MENU----------------------------------------------------------
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.menu_opciones, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        super.onOptionsItemSelected(item);

        switch (item.getItemId()) {
            case (R.id.mNuevaPeli):
                nuevaPelicula();
                return true;

            case (R.id.mAcercaDe):
                Intent intent = new Intent(FilmListActivity.this, AboutActivity.class);
                startActivity(intent);
                return true;

            case (R.id.mCerraSesion):
                signOut();
                return true;

            case (R.id.mDesconectarApp):
                revokeAccess();
                return true;
        }
        return false;
    }

    private void revokeAccess() {
        mGoogleSignInClient.revokeAccess().addOnCompleteListener(this, new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                //HACER ALGO?
                Toast toast1 = Toast.makeText(getApplicationContext(), "Acceso revocado", Toast.LENGTH_SHORT);
                toast1.show();
                finish();
            }
        });
    }

    private void signOut() {
        mGoogleSignInClient.signOut().addOnCompleteListener(this, new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                //HACER ALGO?
                Toast toast1 = Toast.makeText(getApplicationContext(),"Sesión cerrada", Toast.LENGTH_SHORT);
                toast1.show();
                finish();
            }
        });
    }


    private void nuevaPelicula()
    {
        List<Film> peliculas = FilmDataSource.getFilms();
        Film nuevaPeli = new Film();
        nuevaPeli.title = "Titulo por defecto";
        nuevaPeli.director = "Director por defecto";
        nuevaPeli.imageResId = R.mipmap.ic_launcher;
        nuevaPeli.comments = "Comentario por defecto";
        nuevaPeli.format = Film.FORMAT_BLURAY;
        nuevaPeli.genre = Film.GENRE_DRAMA;
        nuevaPeli.imdbUrl = "http://www.imdb.com";
        nuevaPeli.year = 2017;
        nuevaPeli.latitud = 42.358;
        nuevaPeli.longitud = -71.059;

        peliculas.add(nuevaPeli);
        //adaptador = new FilmsArrayAdapter(this, R.layout.item_film, peliculas);
        //FilmDataSource.guardarPeliculasFichero(getApplicationContext());

        adaptador.notifyDataSetChanged(); //para que sepa que hemos añadido una pelicula a la lista
        listViewExterna.setAdapter(adaptador);
        writeDriveFileFilms();
    }



//-----------------------------------------------------------------------------------

    /*class LeerFicheroAsynctask extends AsyncTask<Context, Void, List<Film>>{

        @Override
        protected void onPreExecute(){

        }
        @Override
        protected List<Film> doInBackground(Context... contexts) {
            List <Film> listaPeliculas = FilmDataSource.getFilms(contexts[0]);
            return listaPeliculas;
        }
        @Override
        protected void onPostExecute(List<Film>filmsVar){

        }
    }*/
}
