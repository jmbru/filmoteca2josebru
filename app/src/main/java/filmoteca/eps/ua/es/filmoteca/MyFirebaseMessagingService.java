package filmoteca.eps.ua.es.filmoteca;

import android.app.Activity;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.List;
import java.util.Map;
public class MyFirebaseMessagingService extends FirebaseMessagingService{

    private static final String TAG = "Messaging Service";
    private static final String TAG_ALTA = "Firebase messaging alta";
    private static final String TAG_BAJA = "Firebase messaging baja";

    public static final String ACCION_BAJA = "baja";
    public static final String ACCION_ALTA = "alta";
    public static final String ACCION_NAME = "accion";

    public static final String DATA_TITLE = "title";
    public static final String DATA_DIRECTOR = "director";
    public static final String DATA_YEAR = "year";
    public static final String DATA_GENRE = "genre";
    public static final String DATA_FORMAT = "format";
    public static final String DATA_IMDB = "imdb";
    public static final String DATA_COMMENTS = "comments";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Log.d(TAG, "From: " + remoteMessage.getFrom());

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());
        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
        }

        if (remoteMessage.getData().size() > 0 && remoteMessage.getNotification() != null) {
            String tipoOperacion;
            Map<String, String> datos;

            tipoOperacion = remoteMessage.getNotification().getBody();
            datos = remoteMessage.getData();

            procesarMensaje(tipoOperacion, datos);
        }
    }

    private void procesarMensaje(String tipoOperacion, Map<String, String> datos) {
        switch (tipoOperacion) {
            case ACCION_ALTA:
                accionAltaPelicula(datos);
                break;

            case ACCION_BAJA:
                accionBajoPelicula(datos);
                break;
        }
    }

    private void accionAltaPelicula(Map<String, String> datos) {
        Log.d(TAG_ALTA, "Accion Alta de pelicula");

        List<Film> listaPublicada = FilmDataSource.getFilms();
        boolean existeEnLista = false;

        String title = datos.get(DATA_TITLE);
        String director = datos.get(DATA_DIRECTOR);
        int year = Integer.parseInt(datos.get(DATA_YEAR));
        int genre = Integer.parseInt(datos.get(DATA_GENRE));
        int format = Integer.parseInt(datos.get(DATA_FORMAT));
        String urlImdb = datos.get(DATA_IMDB);
        String comments = datos.get(DATA_COMMENTS);

        Film nuevaPeli = new Film();

        nuevaPeli.setTitle(title);
        nuevaPeli.setDirector(director);
        nuevaPeli.setYear(year);
        nuevaPeli.setGenre(genre);
        nuevaPeli.setFormat(format);
        nuevaPeli.setImdbUrl(urlImdb);
        nuevaPeli.setComments(comments);

        if(listaPublicada!=null){
            for(int i=0; i<listaPublicada.size() && !existeEnLista;i++){
                if(title.equals(listaPublicada.get(i).title)){
                    existeEnLista = true;
                }
            }

            if(existeEnLista){
                Log.d("FirebaseMessaging", "La pelicula ya existia");
            }
            else{
                //llamar al write film in drive
            }

        }

        Message completeMessage = m_handler.obtainMessage(1, nuevaPeli);
        completeMessage.sendToTarget();
    }

    private void accionBajoPelicula(Map<String, String> datos) {
        Log.d(TAG_BAJA, "Accion Baja de pelicula");

        List<Film> listaPublicada = FilmDataSource.getFilms();
        boolean existeEnLista = false;

        String title = datos.get(DATA_TITLE);

        if(listaPublicada!=null){
            for(int i=0; i<listaPublicada.size() && existeEnLista;i++){
                if(title.equals(listaPublicada.get(i).title)){
                    existeEnLista = true;
                    listaPublicada.remove(i);
                }
            }

            if(existeEnLista){
                FilmDataSource.setFilms(listaPublicada);
            }

        }
        Film nuevaPeli = new Film();
        nuevaPeli.setTitle(title);

        Message completeMessage = m_handler.obtainMessage(1, nuevaPeli);
        completeMessage.sendToTarget();
    }

    private Handler m_handler = new Handler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(Message message) {
            // This is where you do your work in the UI thread.
            // Your worker tells you in the message what to do.
            FilmDataSource filmDataSource;
            filmDataSource = new FilmDataSource();
            Log.d("FirebaseMessaging", "Creado film data source");

            Film film = (Film) message.obj;
            Activity activity = new Activity();

            Toast toastPeli = Toast.makeText(getApplicationContext(), film.getTitle().toString(), Toast.LENGTH_SHORT);
            toastPeli.show();

            //filmDataSource.addNewMovieDrive(activity.getBaseContext(), film);
        }
    };

}
