package filmoteca.eps.ua.es.filmoteca;

import android.content.Context;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by mastermoviles on 19/10/17.
 */

public class FilmDataSource {
    public static List<Film> films = new ArrayList<Film>();
    public static List<Film> pelisDefault;

    public static void setFilms(List<Film> films) {
        FilmDataSource.films = films;
    }

    public static List<Film> getFilms(/*Context context*/) {
        /*if(films == null) {
            films = cargarPeliculasFichero(context);
        }*/
        if(films == null) {
            films = pelisDefault;
        }

        return films;
    }

    public static List<Film>cargarPeliculasFichero(Context contexto){
        List<Film> listaPeliculas = FilmDataSource.films;
        File dir = new File(contexto.getFilesDir(), "peliculas.txt");

        FileInputStream fis = null;
        ObjectInputStream ois = null;

        try {
            fis = new FileInputStream(dir);
            ois = new ObjectInputStream(fis);

            listaPeliculas = (List<Film>) ois.readObject();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }finally {
            if(ois!=null)//si no es nulo lo cerramos
            {
                try {
                    ois.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if(fis!=null)
            {
                try {
                    fis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return listaPeliculas;
    }

    public static void guardarPeliculasFichero(Context context) {
        FileOutputStream fos = null;
        ObjectOutputStream oos = null;

        try {
            File file = new File(context.getFilesDir(), "peliculas.txt");
            fos = new FileOutputStream(file);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(films);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if(oos != null)
            {
                try {
                    oos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (fos != null)
            {
                try {
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            //oos.close();

        }
    }

    static{
        pelisDefault = new ArrayList<Film>();

        Film f = new Film();
        f.title = "Regreso al futuro";
        f.director = "Robert Zemekis";
        f.imageResId = R.mipmap.ic_launcher;
        f.comments = "";
        f.format = Film.FORMAT_DIGITAL;
        f.genre = Film.GENRE_SCIFI;
        f.imdbUrl = "http://imdb.com/title/tt0088763";
        f.year = 1985;
        f.latitud = 42.358;
        f.longitud = -71.059;
        pelisDefault.add(f);

        Film f2 = new Film();
        f2.title = "Guerra Mundial Z";
        f2.director = "Marc Forster";
        f2.imageResId = R.mipmap.ic_launcher;
        f2.comments = "";
        f2.format = Film.FORMAT_BLURAY;
        f2.genre = Film.GENRE_ACTION;
        f2.imdbUrl = "http://www.imdb.com/title/tt0816711/?ref_=fn_al_tt_1";
        f2.year = 2013;
        f2.latitud = 440.714;
        f2.longitud = -74.0059;
        pelisDefault.add(f2);

        Film f3 = new Film();
        f3.title = "El club de la lucha";
        f3.director = "David Fincher";
        f3.imageResId = R.mipmap.ic_launcher;
        f3.comments = "";
        f3.format = Film.FORMAT_DVD;
        f3.genre = Film.GENRE_DRAMA;
        f3.imdbUrl = "http://www.imdb.com/title/tt0137523/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2398042102&pf_rd_r=0EB3VC1EGBX1BDR9NQHG&pf_rd_s=center-1&pf_rd_t=15506&pf_rd_i=top&ref_=chttp_tt_10";
        f3.year = 1999;
        f3.latitud = 34.0522;
        f3.longitud = -118.243;
        pelisDefault.add(f3);

        Film f4 = new Film();
        f4.title = "Wiplash";
        f4.director = "Damien Challeze";
        f4.imageResId = R.mipmap.ic_launcher;
        f4.comments = "";
        f4.format = Film.FORMAT_BLURAY;
        f4.genre = Film.GENRE_DRAMA;
        f4.imdbUrl = "http://www.imdb.com/title/tt2582802/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2398042102&pf_rd_r=0EB3VC1EGBX1BDR9NQHG&pf_rd_s=center-1&pf_rd_t=15506&pf_rd_i=top&ref_=chttp_tt_45";
        f4.year = 2014;
        f4.latitud = 40.48935;
        f4.longitud = -3.6827461;
        pelisDefault.add(f4);
    }


}
