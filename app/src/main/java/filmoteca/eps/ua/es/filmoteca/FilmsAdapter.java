package filmoteca.eps.ua.es.filmoteca;


import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

/**
 * Created by mastermoviles on 25/10/17.
 */

public class FilmsAdapter extends RecyclerView.Adapter<FilmsAdapter.ViewHolder> {

    private List<Film> mFilms;

    public FilmsAdapter(List<Film> films) {
        this.mFilms = films;
    }

    public interface OnItemClickListener {
        public void onItemClick(Film f, int position);
    }
    private OnItemClickListener mListener;

    public void setOnItemListener(OnItemClickListener listener) {

        this.mListener = listener;
    }

    public static  class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tiuloPelicula;
        public TextView nombreDirector;
        public ImageView imagenPelicula;

        public ViewHolder(View v){
            super(v);
            tiuloPelicula = (TextView)v.findViewById(R.id.nombrePelicula);
            nombreDirector = (TextView)v.findViewById(R.id.nombreDirector);
            imagenPelicula = (ImageView)v.findViewById(R.id.portadaPelicula);
        }

        public void bind(Film film) {
            tiuloPelicula.setText(film.title);
            nombreDirector.setText(film.director);
            imagenPelicula.setImageResource(film.imageResId);
        }

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_film, parent, false);
        final ViewHolder viewHolder = new ViewHolder(v);
        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int position = viewHolder.getAdapterPosition();
                if (mListener != null) {
                    mListener.onItemClick(mFilms.get(position), position);
                }
            }
        });
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bind(mFilms.get(position));
    }

    @Override
    public int getItemCount() {
        return mFilms.size();
    }
}

