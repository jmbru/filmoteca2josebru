package filmoteca.eps.ua.es.filmoteca;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

import static filmoteca.eps.ua.es.filmoteca.FilmDataActivity.EXTRA_FILM_INDEX;
import static filmoteca.eps.ua.es.filmoteca.FilmDataActivity.EXTRA_FILM_TITLE;

public class FilmEditActivity extends AppCompatActivity {

    //public static final int RESULT_OK = 1;
    public static final String EXTRA_FILM_INDEX = "EXTRA_FILM_INDEX";
    int mIndicePeli;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_film_edit);

        Intent intent = getIntent();
        mIndicePeli = intent.getIntExtra(EXTRA_FILM_INDEX, -1);

        Spinner spinner_genero = (Spinner) findViewById(R.id.spinner_genero_pelicula);
        Spinner spinner_formato = (Spinner) findViewById(R.id.spinner_formato_pelicula);
        /*rellenarSpinner(spinner_genero);
        rellenarSpinner(spinner_formato);*/

        Button button_guardar = (Button)findViewById(R.id.button_guardar);
        Button button_cancelar = (Button) findViewById(R.id.button_cancelar);

        guardar(button_guardar);
        cancelar(button_cancelar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        pintarDatosPantalla();
    }

    void guardar(Button boton) {
        boton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                guardarCambios();
                setResult(RESULT_OK);
                finish();
            }
        });
    }
    void cancelar(Button boton) {
        boton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_CANCELED, null);
                finish();
            }
        });
    }
    /*public void rellenarSpinner(Spinner spinner)
    {
        List<String> lista = new ArrayList<>();
        if(spinner.getId()==R.id.spinner_genero_pelicula)
        {
            lista.add("Accion");
            lista.add("Drama");
            lista.add("Comedia");
            lista.add("Terror");
            lista.add("Sci-fi");

            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, lista);
            spinner.setAdapter(adapter);
        }
        else if(spinner.getId()==R.id.spinner_formato_pelicula)
        {
            lista.add("DVD");
            lista.add("Bluray");
            lista.add("Digital");

            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, lista);
            spinner.setAdapter(adapter);
        }

    }*/

    private void guardarCambios(){

        //ImageView cartel = (ImageView) findViewById(R.id.imageView_cartelPeli);

        List<Film> listaPeliculasAux = FilmDataSource.getFilms(/*getApplicationContext()*/);

        EditText fecha =  (EditText) findViewById(R.id.editText_año_pelicula);
        EditText director =  (EditText) findViewById(R.id.editText_nombre_director);

        Spinner genero = (Spinner) findViewById(R.id.spinner_genero_pelicula);
        Spinner formato = (Spinner) findViewById(R.id.spinner_formato_pelicula);

        EditText titulo = (EditText) findViewById(R.id.editText_titulo_pelicula);
        EditText comentarios = (EditText) findViewById(R.id.editText_comentarios);
        EditText IMDBlink = (EditText) findViewById(R.id.editText_enlace_imdb);

        Film film = listaPeliculasAux.get(mIndicePeli);

        film.title = titulo.getText().toString();
        film.format = formato.getSelectedItemPosition();
        film.genre = genero.getSelectedItemPosition();
        film.year = Integer.parseInt(fecha.getText().toString());
        film.comments = comentarios.getText().toString();
        film.director = director.getText().toString();
        //film.imageResId = cartel.getId();
        film.imdbUrl = IMDBlink.getText().toString();

        //GuardarFicheroAsyncTask guardarFicheroAsyncTask = new GuardarFicheroAsyncTask();
        //guardarFicheroAsyncTask.execute();

        //FilmDataSource.guardarPeliculasFichero(getApplicationContext());
    }

    private void pintarDatosPantalla(){
        List<Film> listaPeliculasAux = FilmDataSource.getFilms(/*getApplicationContext()*/);

        ImageView cartel = (ImageView) findViewById(R.id.imageView_cartelPeli);
        EditText fecha =  (EditText) findViewById(R.id.editText_año_pelicula);
        EditText director =  (EditText) findViewById(R.id.editText_nombre_director);

        Spinner genero = (Spinner) findViewById(R.id.spinner_genero_pelicula);
        Spinner formato = (Spinner) findViewById(R.id.spinner_formato_pelicula);

        EditText titulo = (EditText) findViewById(R.id.editText_titulo_pelicula);
        EditText comentarios = (EditText) findViewById(R.id.editText_comentarios);
        EditText IMDBlink = (EditText) findViewById(R.id.editText_enlace_imdb);


        Film film = listaPeliculasAux.get(mIndicePeli);

        cartel.setImageResource(film.imageResId);
        fecha.setText(Integer.toString(film.year));
        director.setText(film.director);
        titulo.setText(film.title);
        comentarios.setText(film.comments);
        IMDBlink.setText(film.imdbUrl);
        genero.setSelection(film.genre);
        formato.setSelection(film.format);
        /*genero.setText(getResources().getStringArray(R.array.genero)[film.genre]);
        formato.setText(getResources().getStringArray(R.array.formato)[film.format]);
        URLpeli = film.imdbUrl;*/
    }

    class GuardarFicheroAsyncTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... voids) {

            FilmDataSource.guardarPeliculasFichero(getApplicationContext());
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpTo(this, new Intent(this, FilmListActivity.class));
                return true;
        }
        return false;
    }
}
